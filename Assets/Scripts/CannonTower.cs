﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class CannonTower : MonoBehaviour {
	public float m_shootInterval = 0.5f;
	public float m_range = 4f;
	public GameObject m_projectilePrefab;
	public Transform m_shootPoint;

	private float m_lastShotTime = -0.5f;
    public float rotateSpeed = 1f;

    void Update () {
        if (m_projectilePrefab == null || m_shootPoint == null)
            return;

        if (m_lastShotTime + m_shootInterval > Time.time)
            return;

        var monsters = FindObjectsOfType<Monster>();
        var monstersInRange = monsters.Where(m => Vector3.Distance(transform.position, m.transform.position) < m_range);
        var monstersCreatedPriority = monsters.OrderBy(m => int.Parse(m.name));
        foreach(var monster  in monstersCreatedPriority)
        {
            var targetingPosition = monster.transform.position;

            //Высчитываем точку, перед мишенью, по которой нужно произвести выстрел, чтобы попасть по движущейся мишени
            //по идее, чем больше итераций, тем точнее будет положение точки для упреждающего выстрела
            for (int i = 0; i < 10; i++)
            {
                float dist = (m_shootPoint.position - targetingPosition).magnitude;
                float timeToTarget = dist / m_projectilePrefab.GetComponent<CannonProjectile>().m_speed;
                var targetSpeed = (monster.m_moveTarget.transform.position - monster.transform.position).normalized * monster.m_speed;
                var delta = targetSpeed * timeToTarget;
                targetingPosition = monster.transform.position + delta;
                if(delta.magnitude > (monster.m_moveTarget.transform.position - monster.transform.position).magnitude)
                {
                    continue;//пропуск если высчитываемое положение цели после выстрела имеет приращение больше чем до финальной точки движен
                }
            }

            var targetingPosShoot = new Vector3(targetingPosition.x, m_shootPoint.position.y, targetingPosition.z);
            var angle = Vector3.SignedAngle(m_shootPoint.forward.normalized, (targetingPosShoot - m_shootPoint.position).normalized, Vector3.up);
            //угол между поворотом на цель и текущим поворотом
            if (Mathf.Abs(angle) < 1f)
            {
                // shot
                var projectile = Instantiate(m_projectilePrefab, m_shootPoint.position, Quaternion.LookRotation(targetingPosition - m_shootPoint.position)) as GameObject;
                var projectileBeh = projectile.GetComponent<CannonProjectile>();
                
                m_lastShotTime = Time.time;
            }
            else
            {
                if (angle > 1f && angle < 90f)
                {
                    transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
                }
                else if (angle < -1f || angle > 90f)
                {
                    transform.Rotate(Vector3.up, -rotateSpeed * Time.deltaTime);
                }
            }
        }
    }
}
